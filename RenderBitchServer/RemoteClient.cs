﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.ComponentModel;

namespace RenderBitchServer
{
    class RemoteClient : INotifyPropertyChanged
	{
		public event EventHandler ConnectionClosed;
		public event EventHandler ConnectionOpen;
		public event EventHandler<NewMessageEventArgs> NewMessage;
		public event EventHandler<TaskMessageEventArgs> TaskMessage;
		public event EventHandler<TaskMessageEventArgs> NewFrame;
		public event PropertyChangedEventHandler PropertyChanged;

		private static int Counter = 0;
		
		private int _id;
		private int _cpu;
		private int _mem;
        private int _errors;

		private bool _enabled;
		private string _name;
		private string _lastMessage;
		private List<string> _messages;
		private ClientStatus _status;

		public Socket sock;
		public byte[] buffer;
		public string remaining;
		private RemoteTask task;

		public RemoteTask Task
		{
			get { return task; }
			set 
			{ 
				task = value;
				if (value == null && Status != ClientStatus.IDLE)
					task = null;
			}
		}

		public DateTime _lastMessageTime;
		public int _lastFrameID;
		public List<string> MayaVersions;

		public RemoteClient()
		{
			_id = Counter++;
            _enabled = true;
			_cpu = 0;
            _mem = 0;
			_name = "Socket" + _id;
			remaining = "";
			_lastMessage = "";
			_lastMessageTime = new DateTime();
			_status = ClientStatus.IDLE;
			_lastFrameID = -1;
			_messages = new List<string>();
			MayaVersions = new List<string>();
			task = null;
			buffer = new byte[4096];
		}

		public bool Connect(Socket socket)
		{
			sock = socket;
			try
			{
				sock.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None,
					new AsyncCallback(DataCallback), this);
				ConnectionOpen(this, new EventArgs());
				return true;
			}
			catch (System.Exception)
			{
				return false;
			}
		}

		private bool ParseMessage(string m)
		{
			var tokens = m.Split(':');
			_messages.Add(m);
			switch (tokens[0])
			{
				case "CPU":
					_cpu = int.Parse(tokens[1]);
					_mem = int.Parse(tokens[2]);
                    if (PropertyChanged != null)
                    {
						PropertyChanged(this, new PropertyChangedEventArgs("CPU"));
                        PropertyChanged(this, new PropertyChangedEventArgs("MEM"));
                    }
					return true;
				case "NAME":
					_name = tokens[1];
					if (PropertyChanged != null)
						PropertyChanged(this, new PropertyChangedEventArgs("Name"));
					return true;
				case "MAYA":
					var versions = tokens[1].Split(',');
					MayaVersions.AddRange(versions);
					if (PropertyChanged != null)
						PropertyChanged(this, new PropertyChangedEventArgs("VersionsString"));
					return true;
				case "TASK":
					int task_id = int.Parse(tokens[1]);
					//if (Task == null)
					//{
					//	Task = null;
					//}
					if (Task != null && Task.ID == task_id)
					{
						if (tokens[2] == "COMPLETED")
						{
							if (Task.FramesRendered == Task.FramesCount)
							{
								if (Task.Status != RemoteTask.TaskStatus.COMPLETED)
								{
									Task.Status = RemoteTask.TaskStatus.COMPLETED;
									TaskMessage(this, new TaskMessageEventArgs(Task, tokens[2]));
									Status = ClientStatus.IDLE;
									Errors = 0;
									Task = null;
								}
							}
							else
							{
								if (Task.Status != RemoteTask.TaskStatus.FAILED)
								{
									Task.Status = RemoteTask.TaskStatus.FAILED;
									Task.Attempts++;
									Task.Frames.Clear();
									Task.FailedClients.Add(this);
									Errors++;
									Status = Errors > 3 ? ClientStatus.BROKEN : ClientStatus.IDLE;
									TaskMessage(this, new TaskMessageEventArgs(Task, "FAILED"));
									Task = null;
								}
							}
						}
						else if (tokens[2] == "FAILED")
						{
							Task.Status = RemoteTask.TaskStatus.FAILED;
							Task.Attempts++;
							Task.Frames.Clear();
							Task.FailedClients.Add(this);
							Errors++;
							Status = Errors > 3 ?  ClientStatus.BROKEN : ClientStatus.IDLE;
							TaskMessage(this, new TaskMessageEventArgs(Task, tokens[2]));
							Task = null;
						}
						else if (tokens[2] == "PROGRESS")
						{
							Task.ProgressFrame = int.Parse(tokens[3]);
						}
						else if (tokens[2] == "FRAME")
						{
							Task.ProgressFrame = 0;
							Task.Frames.Add(tokens[3]);
							_lastFrameID = task_id;
							NewFrame(this, new TaskMessageEventArgs(Task, tokens[3]));
						}
					}
					return true;
				default:
					break;
			}
			return false; // Message not handled
		}

		private void DataCallback(IAsyncResult ar)
		{
			try
			{
				int bytesRead = sock.EndReceive(ar);
				
				if (bytesRead == 0)
				{
					sock.Close();
					buffer = null;
					sock = null;
					ConnectionClosed(this, new EventArgs());
					return;
				}

				_lastMessageTime = DateTime.Now;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("LastMessage"));
                int start = 0;
				string s = remaining + Encoding.ASCII.GetString(buffer, 0, bytesRead);
				while (bytesRead > start)
				{
					int index = s.IndexOf("\a\r\n", start);
					if (index >= 0)
					{
						int len = index - start;
						string message = s.Substring(start, len);
						start += len + 3;
						// Try to parse the message and forward if necessary to the main server
						if (!ParseMessage(message))
						{
							_lastMessage = message;
							if (PropertyChanged != null)
								PropertyChanged(this, new PropertyChangedEventArgs("LastMessage"));
							NewMessage(this, new NewMessageEventArgs(message));
						}
					}
					else
					{
						remaining = s.Substring(start, bytesRead - start);
						break;
					}
				}
				sock.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None,
					new AsyncCallback(DataCallback), this);
			}
			catch (System.Exception)
			{
				sock.Close();
				buffer = null;
				sock = null;
				ConnectionClosed(this, new EventArgs());
			}
		}

		public void Send(string message)
		{
			sock.Send(Encoding.ASCII.GetBytes(message + "\a\r\n"));
		}

		public bool SocketConnected()
		{
			bool part1 = sock.Poll(1000, SelectMode.SelectRead);
			bool part2 = (sock.Available == 0);
			if (part1 && part2)
				return false;
			else
				return true;
		}

		public bool Alive 
		{ 
			get 
			{
				TimeSpan diff = DateTime.Now - _lastMessageTime;
                return diff.TotalSeconds < 10;
			} 
		}

		public ClientStatus Status
		{
			get { return _status; }
			set
			{
				_status = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Status"));
			}
		}

		public bool Enabled
		{
			get { return _enabled; }
			set 
			{
				_enabled = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Enabled"));
			}
		}
		
		public int Errors
		{
			get { return _errors; }
			set
			{
				_errors = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Errors"));
			}
		}

		public string Name { get { return _name; } }

		public int ID { get { return _id; } }

		public int CPU { get { return _cpu; } }

        public int MEM { get { return _mem; } }

        public string LastMessage
        {
            get
            {
                TimeSpan ts = _lastMessageTime.TimeOfDay;
                return string.Format("{0:D}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
        }

		public string VersionsString { get { return string.Join(",", MayaVersions); } }

		public class NewMessageEventArgs : EventArgs
		{
			public NewMessageEventArgs(string msg)
			{
				Message = msg;
			}
			public string Message;
		}

		public class TaskMessageEventArgs : EventArgs
		{
			public TaskMessageEventArgs(RemoteTask t, string msg)
			{
				task = t;
				message = msg;
			}
			public string message;
			public RemoteTask task;
		}

		public enum ClientStatus
		{
			IDLE, BUSY, WORKING, BROKEN
		}
	}
}
