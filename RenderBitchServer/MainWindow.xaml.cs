﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using System.Threading;

namespace RenderBitchServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
	{
		Mutex mutex;
		ObservableCollection<RemoteClient> clients;
		ObservableCollection<RemoteJob> jobs;
		ObservableCollection<RemoteTask> tasks;
		DispatcherTimer refreshTimer;

		public MainWindow()
		{
			InitializeComponent();
			Loaded += MainWindow_Loaded;
		}

		void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, 15000);
			Socket listener = new Socket(AddressFamily.InterNetwork,
				SocketType.Stream, ProtocolType.Tcp);
			listener.Bind(endpoint);
			listener.Listen(1000);
			listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);

			Log("startup");
			mutex = new Mutex();

			clients = new ObservableCollection<RemoteClient>();
			clientsGrid.ItemsSource = clients;

			jobs = new ObservableCollection<RemoteJob>();
			jobsGrid.ItemsSource = jobs;

			tasks = new ObservableCollection<RemoteTask>();
			tasksGrid.ItemsSource = tasks;

			refreshTimer = new DispatcherTimer();
			refreshTimer.Tick += refreshTimer_Tick;
			refreshTimer.Interval = new TimeSpan(0, 0, 1);
			refreshTimer.Start();
		}

		void refreshTimer_Tick(object sender, EventArgs e)
		{
			mutex.WaitOne();
			foreach (var c in clients)
			{
				if (!c.Alive)
				{
					//clients.Remove(c);
					c.sock.Close();
				}
			}
			foreach (var j in jobs)
			{
				foreach (var c in clients)
				{
					if (c.Enabled && c.Status == RemoteClient.ClientStatus.IDLE)
					{
						var t = j.GetReadyTask();
						if (t != null && c != null && c.SocketConnected() && !t.FailedClients.Contains(c))
						{
							t.Client = c.Name;
							c.Task = t;
							c.Send(t.Command);
							c.Status = RemoteClient.ClientStatus.WORKING;
							t.Status = RemoteTask.TaskStatus.ASSIGNED;
                            t.StartTimer();
							if (c.Task == null)
							{
								c.Task = null;
							}
						}
					}
				}
			}
			mutex.ReleaseMutex();
		}

		public void Log(string s)
		{
			Dispatcher.Invoke(new Action(() => { txtLog.Text += s + "\n"; }));
		}

		private void AcceptCallback(IAsyncResult ar)
		{
			Socket listener = ar.AsyncState as Socket;
			Socket handler = listener.EndAccept(ar);
			RemoteClient client = new RemoteClient();
			client.ConnectionClosed += client_ConnectionClosed;
			client.ConnectionOpen += client_ConnectionOpen;
			client.NewMessage += client_NewMessage;
			client.NewFrame += client_NewFrame;
			client.TaskMessage += client_TaskMessage;
			client.Connect(handler);
			listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
		}

		void client_NewFrame(object sender, RemoteClient.TaskMessageEventArgs e)
		{
			RemoteClient client = sender as RemoteClient;
			//Log("FRAME: " + client.Name + " " + e.message);
		}

		void client_TaskMessage(object sender, RemoteClient.TaskMessageEventArgs e)
		{
			RemoteClient client = sender as RemoteClient;
			Dispatcher.Invoke(new Action(() =>
			{
				mutex.WaitOne();
				foreach (RemoteJob j in jobs.Where(x => x.Tasks.Contains(e.task)))
				{
					if (e.message == "COMPLETED" && j != null && e.task != null)
					{
						Log("OK: " + client.Name + " " + e.task.FrameStart + " - " + e.task.FrameEnd);
						j.CompletedTasks++;
					}
					else if (e.message == "FAILED" && e.task != null)
					{
						Log("ER: " + client.Name + " " + e.task.FrameStart + " - " + e.task.FrameEnd);
					}
				}
				mutex.ReleaseMutex();
			}));
		}

		void client_ConnectionOpen(object sender, EventArgs e)
		{
			RemoteClient client = sender as RemoteClient;
			Log(client.Name + " accepted");
			Dispatcher.Invoke(new Action(() => { 
				clients.Add(client);
			}));
		}

		void client_NewMessage(object sender, RemoteClient.NewMessageEventArgs e)
		{
			RemoteClient client = sender as RemoteClient;
		}

		void client_ConnectionClosed(object sender, EventArgs e)
		{
			RemoteClient client = sender as RemoteClient;
			Log(client.Name + " closed");
			Dispatcher.Invoke(new Action(() =>
			{
				if (client.Task != null)
				{
					client.Task.Status = RemoteTask.TaskStatus.READY;
					client.Task.ProgressFrame = 0;
					client.Task.Frames.Clear();
				}
				clients.Remove(client);
			}));
		}

		private void btnSend_Click(object sender, RoutedEventArgs e)
		{
			foreach (var c in clients)
				c.Send(txtCommand.Text);
		}

		private void btnSend_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				foreach (var c in clients)
					c.Send(txtCommand.Text);
			}
		}

		private void btnOpen_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog d = new OpenFileDialog();
			d.Filter = "Maya Project (*.ma;*.mb)|*.ma;*.mb";
			if (d.ShowDialog() == true)
			{
				OpenScene(d.FileName);
			}
		}

		private void OpenScene(string filename)
		{
			FileInfo fi = new FileInfo(filename);
			if (!fi.Exists)
				return;

			txtFilename.Text = filename;
			if (fi.Directory.Name == "scenes")
			{
				txtProjectPath.Text = fi.Directory.Parent.FullName;
				txtOutPath.Text = txtProjectPath.Text + "\\images";
			}
			else
			{
				txtProjectPath.Text = fi.Directory.FullName;
				txtOutPath.Text = txtProjectPath.Text;
			}
			if (chkParse.IsChecked.Value && fi.Extension == ".ma")
			{
				var sr = new StreamReader(fi.OpenRead());
				string line = null;
				string reparse = null;
                listCameras.Items.Clear();
				while (reparse != null || (line = sr.ReadLine()) != null)
				{
					if (reparse != null)
						line = reparse;
					reparse = null;
					if (line.StartsWith("createNode camera"))
					{
						Regex r = new Regex("-p \"(\\w+)\"");
						List<string> ignore = new List<string>(new string[] { "persp", "top", "front", "side" });
						var m = r.Match(line);
						if (m.Success && !ignore.Contains(m.Groups[1].Value))
						{
							listCameras.Items.Add(m.Groups[1].Value);
							listCameras.SelectedIndex = 0;
						}
					}
					else if (line.StartsWith("select -ne :defaultResolution;"))
					{
						Regex r = new Regex("setAttr \"(\\.\\w)\" ([^;]+);");
						while ((line = sr.ReadLine()) != null)
						{
							var m = r.Match(line);
							if (m.Success)
							{
								if (m.Groups[1].Value == ".w")
									txtResX.Text = m.Groups[2].Value;
								else if (m.Groups[1].Value == ".h")
									txtResY.Text = m.Groups[2].Value;
							}
							else
							{
								reparse = line;
								break;
							}
						}
					}
					else if (line.StartsWith("select -ne :defaultRenderGlobals;"))
					{
						Regex r = new Regex("setAttr \"(\\.\\w*)\" ([^;]+);");
						while ((line = sr.ReadLine()) != null)
						{
							var m = r.Match(line);
							if (m.Success)
							{
								if (m.Groups[1].Value == ".fs")
									txtFrameStart.Text = m.Groups[2].Value;
								else if (m.Groups[1].Value == ".ef")
									txtFrameEnd.Text = m.Groups[2].Value;
							}
							else
							{
								reparse = line;
								break;
							}
						}
					}
				}
			}
		}

		private void btnCreateJob_Click(object sender, RoutedEventArgs e)
		{
			RemoteJob job = new RemoteJob();
			FileInfo fi = new FileInfo(txtFilename.Text);
			job._file = fi.Name;
			job._path = fi.FullName;
			job._proj = txtProjectPath.Text;
			job._outd = txtOutPath.Text;
			job._camera = listCameras.Text;
			job._taskSize = int.Parse(txtTaskSize.Text);
			job._resx = int.Parse(txtResX.Text);
			job._resy = int.Parse(txtResY.Text);
			job._resPercent = int.Parse(cmbResPercent.Text);
			job._frameStart = int.Parse(txtFrameStart.Text);
			job._frameEnd = int.Parse(txtFrameEnd.Text);
			job._totalFrames = job._frameEnd - job._frameStart + 1;
			job._totalTasks = (int)Math.Ceiling((float)job._totalFrames / job._taskSize);
			job.CreateTasks();

            if (chkAffinity.IsChecked.Value)
            {
                job.ClientsAffinity = new List<RemoteClient>();
                foreach (RemoteClient c in clientsGrid.SelectedItems)
                {
                    job.ClientsAffinity.Add(c);
                }
            }

			foreach(var t in job.Tasks)
				tasks.Add(t);
			
			jobs.Add(job);
		}

		private void btnDeleteJobs_Click(object sender, RoutedEventArgs e)
		{
			foreach (var c in clients)
				if (c.Task != null)
					c.Send("TASK:" + c.Task.ID + ":KILL:");
			foreach (var j in jobs)
				foreach (var t in j.Tasks)
					tasks.Remove(t);
			jobs.Clear();
			txtLog.Text = "Log\n";
		}

		private void btnKillClients_Click(object sender, RoutedEventArgs e)
		{
			foreach (RemoteClient c in clientsGrid.SelectedItems)
				c.Send("SYS:EXIT");
		}

		private void btnRestartClients_Click(object sender, RoutedEventArgs e)
		{
			foreach (var c in clients)
				c.Send("SYS:RESTART");
		}

        private void btnRestartTasks_Click(object sender, RoutedEventArgs e)
        {
            foreach (RemoteTask task in tasksGrid.SelectedItems)
            {
                //var task = cell.Item as RemoteTask;
                var job = jobs.Where(x => x.Tasks.Contains(task)).First();
                if (task.Status == RemoteTask.TaskStatus.COMPLETED)
                    job.CompletedTasks--;
                task.Attempts = 0;
                task.ProgressFrame = 0;
                task.Frames.Clear();
                task.FailedClients.Clear();
                task.Status = RemoteTask.TaskStatus.READY;
            }
        }

        private void jobsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<RemoteTask> tasks = new List<RemoteTask>();
            foreach (RemoteJob job in jobsGrid.SelectedItems)
                tasks.AddRange(job.Tasks);
            tasksGrid.ItemsSource = tasks;
        }

        private void allGrids_MouseUp(object sender, MouseButtonEventArgs e)
        {
            // Deselect when clicked a blank area
            if (sender != null)
            {
                DataGrid grid = sender as DataGrid;
                if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                {
                    DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                    if (!dgr.IsMouseOver)
                    {
                        (dgr as DataGridRow).IsSelected = false;
                        grid.SelectedItem = null;
                        grid.UnselectAll();
                        grid.UnselectAllCells();
                    }
                }
            }
        }

        private void btnShutdownClients_Click(object sender, RoutedEventArgs e)
        {
            foreach (RemoteClient c in clientsGrid.SelectedItems)
                c.Send("SYS:SHUTDOWN");
        }
    }
}
