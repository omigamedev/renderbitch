﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace RenderBitchServer
{
    class RemoteJob : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		public static int _counter = 0;
		public int _id;
		public string _name;
		public string _file;
		public int _totalFrames;
		public int _totalTasks;
		public int _progress;
		public int _frameStart;
		public int _frameEnd;
		public int _resx;
		public int _resy;
		public int _resPercent;
		public int _taskSize;
		public int _renderThreads;
        public DateTime _startTime;
        public DateTime _endTime;
        public string _path;
		public string _proj;
		public string _outd;
		public string _camera;
        public List<RemoteClient> ClientsAffinity;
		public ObservableCollection<RemoteTask> Tasks;

		private int _completedTasks;

		public RemoteJob()
		{
			_id = _counter++;
			_progress = 0;
			_completedTasks = 0;
			_totalTasks = 0;
			_renderThreads = 0;
			_name = "Job" + _id;
            _startTime = _endTime = DateTime.Now;
		}

		public void CreateTasks()
		{
			Tasks = new ObservableCollection<RemoteTask>();
			Tasks.CollectionChanged += Tasks_CollectionChanged;
			float step = (float)_totalFrames / _totalTasks;
			float offset = -1;
			int fs = 0;
			while (fs <_totalFrames)
			{
				offset += step;
				int fe = (int)Math.Round(offset);

				var rt = new RemoteTask();
				rt.Command = GenerateTaskCommand(rt.ID, fs, fe);
				rt.FrameStart = fs;
				rt.FrameEnd = fe;
                rt.JID = _id;
				Tasks.Add(rt);

				fs = fe + 1;
			}
		}

		private void Tasks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("Progress"));
        }

        public RemoteTask GetReadyTask()
		{
			foreach (var t in Tasks)
				if (t.Status == RemoteTask.TaskStatus.READY || 
					(t.Status == RemoteTask.TaskStatus.FAILED && t.Attempts < 5))
					return t;
			return null;
		}

		public void ParseMessage(string msg)
		{

		}

		public string GenerateTaskCommand(int id, int FrameStart, int FrameEnd)
		{
			string cmd = "TASK:" + id + ":RUN:"
				+ " -s " + (_frameStart + FrameStart)
				+ " -e " + (_frameStart + FrameEnd)
				+ " -r mr -v 5"
				+ " -x " + _resx
				+ " -y " + _resy
				+ " -percentRes " + _resPercent
				+ " -proj \"" + _proj + "\""
				+ " -rd \"" + _outd + "\""
				+ " -cam " + _camera
				+ " -rt " + _renderThreads
				+ " \"" + _path + "\"";
			return cmd;//"TASK:" + id + ":RUN: -help";
		}

		public string Camera
		{
			get { return _camera; }
		}

		public int TotalTasks
		{
			get { return _totalTasks; }
		}

		public int ID
		{
			get { return _id; }
		}

		public string Name
		{
			get { return _name; }
		}

		public string File
		{
			get { return _file; }
		}

		public int TotalFrames
		{
			get { return _totalFrames; }
		}

		public int CompletedTasks
		{
			get { return _completedTasks; }
			set
			{
				_completedTasks = value; 
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("CompletedTasks"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Progress"));
                    PropertyChanged(this, new PropertyChangedEventArgs("ElapsedTime"));
                }
                _endTime = DateTime.Now;
            }
        }

		public int Progress
		{
			get { return (int)(((float)_completedTasks / Tasks.Count) * 100.0f); }
		}

        public string ElapsedTime
        {
            get
            {
                TimeSpan ts = _endTime - _startTime;
                return string.Format("{0:D}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
        }
	}

	class RemoteTask : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public List<RemoteClient> FailedClients;

		private static int _counter;
		private int _ID;
		private int _JID;
        private string _Command;
		private int _FrameStart;
		private int _ProgressFrame;
		private TaskStatus _Status;
		private string _Client;
		private int _Attempts;
		private int _FrameEnd;
        public DateTime _startTime;
        public DateTime _endTime;
        public ObservableCollection<string> Frames;

		public RemoteTask()
		{
			ID = _counter++;
            JID = -1;
			Command = "";
			Client = "";
			Attempts = 0;
			ProgressFrame = 0;
			Frames = new ObservableCollection<string>();
			Frames.CollectionChanged += Frames_CollectionChanged;
			FailedClients = new List<RemoteClient>();
			Status = TaskStatus.READY;
		}

        public void StartTimer()
        {
            _startTime = _endTime = DateTime.Now;
        }

        void Frames_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs("FramesRendered"));
				PropertyChanged(this, new PropertyChangedEventArgs("ProgressTask"));
                PropertyChanged(this, new PropertyChangedEventArgs("ElapsedTime"));
            }
            _endTime = DateTime.Now;
        }

        public enum TaskStatus
		{
			READY, ASSIGNED, FAILED, CORRUPTED, COMPLETED
		}

		public string Client
		{
			get { return _Client; }
			set
			{
				_Client = value;
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Client"));
			}
		}

		public int FramesRendered
		{
			get { return Frames.Count; }
		}

		public int ID
		{
			get { return _ID; }
			set
			{
				_ID = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("ID"));
			}
		}

        public int JID
        {
            get { return _JID; }
            set
            {
                _JID = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("JID"));
            }
        }

        public string Command
		{
			get { return _Command; }
			set
			{
				_Command = value;
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Command"));
			}
		}

		public int Attempts
		{
			get { return _Attempts; }
			set
			{
				_Attempts = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Attempts"));
			}
		}

		public int FrameStart
		{
			get { return _FrameStart; }
			set
			{
				_FrameStart = value;
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("FrameStart"));
			}
		}

		public int FrameEnd
		{
			get { return _FrameEnd; }
			set
			{
				_FrameEnd = value;
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("FrameEnd"));
			}
		}

		public int FramesCount
		{
			get { return FrameEnd - FrameStart + 1; }
		}

		public int ProgressTask
		{
			get { return (int)((float)(Frames.Count * 100.0f + ProgressFrame) / FramesCount); }
		}

		public int ProgressFrame
		{
			get { return _ProgressFrame; }
			set
			{
				_ProgressFrame = value; 
				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("ProgressFrame"));
					PropertyChanged(this, new PropertyChangedEventArgs("ProgressTask"));
                    PropertyChanged(this, new PropertyChangedEventArgs("ElapsedTime"));
                }
                _endTime = DateTime.Now;
            }
        }

		public TaskStatus Status
		{
			get { return _Status; }
			set
			{
				_Status = value; 
				if (PropertyChanged != null)
					PropertyChanged(this, new PropertyChangedEventArgs("Status"));
			}
		}

        public string ElapsedTime
        {
            get
            {
                if (Status == TaskStatus.READY)
                    return "";
                TimeSpan ts = _endTime - _startTime;
                return string.Format("{0:D}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
        }
    }
}
