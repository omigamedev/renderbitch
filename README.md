# RenderBitch - Distributed Network Rendering Manager #

This project is developed to help in the rendering process when using multiple heterogeneous machines. The server runs on a Windows client and it's written in C# which gives a easy way to build an intuitive front-end to submit the jobs. The clients are simple console applications that uses boost library to access the network and communicate with the server via TCP socket.

This is the client running on a Windows 8.1 PC. It detects the versions of the software installed on the machine and remains idle looking for a server to be available.

![rbclient_1.png](https://bitbucket.org/repo/MArXBz/images/1219951546-rbclient_1.png)

When the client is connected it waits for a job and starts the execution of the process parsing its output.

![rbclient-job1_1.png](https://bitbucket.org/repo/MArXBz/images/782864545-rbclient-job1_1.png)

The server appears with a form section where we can select the file to render. It works best with .ma (Maya ASCII) that can be parsed looking for useful information to automatically fill the form.

![rbserver-open_2.png](https://bitbucket.org/repo/MArXBz/images/2414472610-rbserver-open_2.png)

After launching the job we can monitor the progress and the work load for each client connected.

![rbserver-job_2.png](https://bitbucket.org/repo/MArXBz/images/1238406811-rbserver-job_2.png)