#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <boost/asio.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>

#include "targetver.h"
#include <windows.h>
#include <windowsx.h>
#include <shellapi.h>

#ifdef _WIN32
#endif

#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h>

#include <condition_variable>
#include <string>
#include <thread>
#include <queue>
#include <mutex>
#include <regex>
