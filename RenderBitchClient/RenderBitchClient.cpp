#include "stdafx.h"

using namespace boost;
using asio::ip::tcp;

asio::io_service io_service;
tcp::resolver resolver(io_service);
std::shared_ptr<tcp::socket> sock;

std::queue<std::string> send_queue;
std::mutex send_mutex;
std::condition_variable send_cv;

std::string maya_versions_string;
std::vector<std::string> maya_versions;


void post_message(const char* format, ...)
{
	static char buffer[4096];
	{
		std::lock_guard<std::mutex> lock(send_mutex);
		va_list args;
		va_start(args, format);
		vsprintf(buffer, format, args);
		//printf("%s\n", buffer);
		send_queue.push(buffer);
		va_end(args);
	}
	send_cv.notify_all();
}

struct Task
{
	static int _counter;
	int id;
	bool running;
	std::string program;
	std::thread t;
	HANDLE g_hChildStd_OUT_Rd = NULL;
	PROCESS_INFORMATION pi;

	Task(int task_id)
	{
		id = task_id;
	}

	~Task()
	{
		//if (t.joinable())
		//	t.detach();
	}

	virtual void ParseOutput(std::string line) = 0;

	bool Start(std::string args)
	{
		std::string path = program + " " + args;

		HANDLE g_hChildStd_OUT_Wr = NULL;

		SECURITY_ATTRIBUTES sa;
		sa.nLength = sizeof(SECURITY_ATTRIBUTES);
		sa.bInheritHandle = TRUE;
		sa.lpSecurityDescriptor = NULL;

		CreatePipe(&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &sa, 0);
		SetHandleInformation(g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0);

		ZeroMemory(&pi, sizeof(pi));

		STARTUPINFO si;
		ZeroMemory(&si, sizeof(si));
		si.cb         = sizeof(si);
		si.hStdError  = g_hChildStd_OUT_Wr;
		si.hStdOutput = g_hChildStd_OUT_Wr;
		si.dwFlags   |= STARTF_USESTDHANDLES;

		bool bSuccess = CreateProcess(
			NULL,
			(char*)path.c_str(),  // command line 
			NULL,                 // process security attributes 
			NULL,                 // primary thread security attributes 
			TRUE,                 // handles are inherited 
			DETACHED_PROCESS,                    // creation flags 
			NULL,                 // use parent's environment 
			NULL,                 // use parent's current directory 
			&si,                  // STARTUPINFO pointer 
			&pi);                 // receives PROCESS_INFORMATION 

		//CloseHandle(g_hChildStd_OUT_Rd);
		CloseHandle(g_hChildStd_OUT_Wr);

		if (!bSuccess)
		{
			printf("error in CreateProcess\n");
			return false;
		}

		//post_message("TASK:%d:VERSION:%s", id, maya_versions[0]);

		printf("task created\n");
		running = true;
		t = std::thread(&Task::Run, this);
		t.detach();

		return true;
	}

	void Kill()
	{
		if (running)
		{
			running = false;
			if (TerminateProcess(pi.hProcess, -1))
				printf("TERMINATE\n");
			else
				printf("termination failed");
		}
	}

	void Run()
	{
		CHAR chBuf[4097];
		DWORD bytesRead;
		std::string remaining;
		while (running && ReadFile(g_hChildStd_OUT_Rd, chBuf, 4096, &bytesRead, nullptr))
		{
			chBuf[bytesRead] = 0;

			std::string s = remaining + chBuf;
			std::replace(s.begin(), s.end(), '\r', ' ');

			int index;
			while ((index = s.find("\n")) >= 0)
			{
				std::string message = s.substr(0, index - 1);
				if (!message.empty())
					ParseOutput(message);
				s = s.substr(index + 1, s.size());
			}
			remaining = s;
		}
		if (!remaining.empty())
			ParseOutput(remaining);

		if (running)
			WaitForSingleObject(pi.hProcess, INFINITE);

		DWORD exitCode = 0;
		if (!GetExitCodeProcess(pi.hProcess, &exitCode))
		{
			exitCode = -1;
			//t.detach();
			printf("task terminated\n");
		}
		else
		{
			CloseHandle(g_hChildStd_OUT_Rd);
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
			printf("task exited with %d\n", exitCode);
		}

		//for (int i = 0; i < 3; i++)
		{
			if (exitCode == 0)
				post_message("TASK:%d:COMPLETED", id);
			else
				post_message("TASK:%d:FAILED", id);
		}

		running = false;
	}
};
int Task::_counter = 0;

struct MayaTask : public Task
{
	std::regex reg_progress;
	std::regex reg_image;
	std::regex reg_completed;
	MayaTask(int task_id, std::string version) : Task(task_id),
		reg_progress(R"(progr:\s+(\d+\.\d)%)"),
		reg_image(R"(writing frame buffer (mayaColor) to image file (.*) \(frame \d+\))"),
		reg_completed(R"(Scene (.*) completed\.)")
	{
		char str[256];
		sprintf(str, R"("C:\Program Files\Autodesk\Maya%s\bin\Render.exe")", version.c_str());
		program = str;
	}

	virtual void ParseOutput(std::string line) override
	{
		static auto start = std::chrono::steady_clock::now();
		auto end = std::chrono::steady_clock::now();
		float dt = std::chrono::duration<float>(end - start).count();
		start = end;
		std::smatch m;
		if (std::regex_search(line, m, reg_progress))
		{
			static float timer = 0;
			timer += dt;
			float p = atof(m[1].str().c_str());
			if (timer > 0.5f || p == 100.f)
			{
				printf("out: %s\n", line.c_str());
				post_message("TASK:%d:PROGRESS:%d", id, (int)p);
				timer = 0;
			}
		}
		else if (std::regex_search(line, m, reg_image))
		{
			printf("out: %s\n", line.c_str());
			post_message("TASK:%d:FRAME:%s", id, m[2].str().c_str());
		}
		else if (std::regex_search(line, m, reg_completed))
		{
			printf("out: %s\n", line.c_str());
			post_message("TASK:%d:COMPLETED", id);
		}
		else
		{
			printf("out: %s\n", line.c_str());
		}

	}
};


//// Link: http://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
// OLD METHOD

//----------------------------------------------------------------------------------------------------------------
// cpuusage(void)
// ==============
// Return a CHAR value in the range 0 - 100 representing actual CPU usage in percent.
//----------------------------------------------------------------------------------------------------------------
CHAR cpuusage()
{
	FILETIME ft_sys_idle;
	FILETIME ft_sys_kernel;
	FILETIME ft_sys_user;

	ULARGE_INTEGER ul_sys_idle;
	ULARGE_INTEGER ul_sys_kernel;
	ULARGE_INTEGER ul_sys_user;

	static ULARGE_INTEGER ul_sys_idle_old;
	static ULARGE_INTEGER ul_sys_kernel_old;
	static ULARGE_INTEGER ul_sys_user_old;

	GetSystemTimes(&ft_sys_idle, &ft_sys_kernel, &ft_sys_user);

	CopyMemory(&ul_sys_idle  , &ft_sys_idle  , sizeof(FILETIME));
	CopyMemory(&ul_sys_kernel, &ft_sys_kernel, sizeof(FILETIME));
	CopyMemory(&ul_sys_user  , &ft_sys_user  , sizeof(FILETIME));

	auto dt_kern = ul_sys_kernel.QuadPart - ul_sys_kernel_old.QuadPart;
	auto dt_user = ul_sys_user.QuadPart - ul_sys_user_old.QuadPart;
	auto dt_idle = ul_sys_idle.QuadPart - ul_sys_idle_old.QuadPart;
	CHAR usage = ((dt_kern + dt_user - (dt_idle)) * 100) / (dt_kern + dt_user);

	ul_sys_idle_old.QuadPart   = ul_sys_idle.QuadPart;
	ul_sys_user_old.QuadPart   = ul_sys_user.QuadPart;
	ul_sys_kernel_old.QuadPart = ul_sys_kernel.QuadPart;

	return usage;
}

int memusage()
{
	MEMORYSTATUSEX statex;
	statex.dwLength = sizeof(statex);
	GlobalMemoryStatusEx(&statex);
	return statex.dwMemoryLoad;
}

inline bool file_exists(const std::string& name) 
{
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}

int main(int argc, char* argv[])
{
	printf("Welcome to RenderBitch Client\n\n");
	auto endpoint_it_omar = resolver.resolve({ "192.168.127.150", "15000" });
	auto endpoint_it_ernest = resolver.resolve({ "192.168.127.196", "15000" });

	std::vector<decltype(endpoint_it_omar)> endpoints;
	endpoints.push_back(endpoint_it_omar);
	endpoints.push_back(endpoint_it_ernest);

	Task* currentTask = nullptr;

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;

	char* root_dir = "C:\\Program Files\\Autodesk";
	for (int i = 1; i < argc-1; i++)
	{
		if (argv[i][0] == '-')
		{
			char* param = strtok(&argv[i][1], "=");
			char* value = strtok(nullptr, "");

			if (strcmp(param, "maya_root") == 0 && value != nullptr)
			{
				root_dir = value;
			}
		}
	}

	char search[256]{};
	sprintf(search, "%s\\*", root_dir);
	printf("Searching in '%s'\n", root_dir);
	std::regex reg_mayaver{ "Maya(\\d+)" };
	hFind = FindFirstFile(search, &FindFileData);
	do
	{
		if (hFind == INVALID_HANDLE_VALUE)
		{
			printf("FindFirstFile failed (%d)\n", GetLastError());
			break;
		}
		else
		{
			//printf(TEXT("The first file found is %s\n"), FindFileData.cFileName);
			std::string dir(FindFileData.cFileName);
			std::string renderer = std::string(root_dir) + "\\" + dir + "\\bin\\Render.exe";
			std::smatch m;
			if (std::regex_match(dir, m, reg_mayaver) && file_exists(renderer))
			{
				if (!maya_versions_string.empty())
					maya_versions_string += ",";
				maya_versions_string += m[1].str();
				maya_versions.push_back(m[1].str());
			}
		}
	}
	while (FindNextFile(hFind, &FindFileData) != 0);
	FindClose(hFind);

	if (maya_versions_string.empty())
	{
		printf("Error: no maya versions found\n");
		std::system("pause");
		exit(0);
	}

	printf("MayaVersions: %s\n", maya_versions_string.c_str());
	printf("Using: Maya%s\n", maya_versions.front().c_str());

	bool reconnecting = false;
	bool running = true;
	int currentEndpoint = 0;
	while (running)
	{
		boost::system::error_code err;
		
		if (!reconnecting)
			printf("connecting... ");
		reconnecting = true;

		sock.reset(new tcp::socket(io_service));
		asio::connect(*sock, endpoints[currentEndpoint], err);
		currentEndpoint = (currentEndpoint + 1) % endpoints.size();
		asio::streambuf response;
		if (!err)
		{
			char str[256];

			printf("connected\n");
			bool connected = true;
			reconnecting = false;

			char computer_name[64];
			gethostname(computer_name, sizeof(computer_name));
			post_message("NAME:%s", computer_name);

			post_message("MAYA:%s", maya_versions.front().c_str());

			std::thread sender_thread([&](){
				while (connected)
				{
					std::unique_lock<std::mutex> lock(send_mutex);
					if (send_queue.size() == 0)
						send_cv.wait(lock, [&]{ return send_queue.size() > 0 || !connected; });
					if (connected)
					{
						asio::write(*sock, asio::buffer(send_queue.front() + "\a\r\n"));
						//printf("SEND: %s\n", send_queue.front().c_str());
						send_queue.pop();
					}
				}
			});

			std::thread usage_thread([&]{
				using namespace boost::accumulators;
				int step = 20;
				int count = 0;
				boost::circular_buffer<int> buf(step);

				while (connected)
				{
					int usage = cpuusage();
					int mem = memusage();
					buf.push_back(usage);

					if (count++ >= step)
					{
						count = 0;
						float avg = 0;
						for (int n : buf)
							avg += n;
						post_message("CPU:%d:%d", (int)(avg / buf.size()), mem);
					}
					std::this_thread::sleep_for(std::chrono::milliseconds(100));
				}
			});

			size_t readBytes;
			while (running && (readBytes = asio::read_until(*sock, response, "\a\r\n", err)))
			{
				static char read_buffer[4096];
				if (!err)
				{
					std::istream response_stream(&response);
					response_stream.read(read_buffer, readBytes);
					char* cmd = strtok(read_buffer, ":");
					if (strcmp(cmd, "SYS") == 0)
					{
						char* args = strtok(nullptr, "\a\r\n");
						if (strcmp(args, "EXIT") == 0)
						{
							sock->close();
							connected = false;
							running = false;
						}
						else if (strcmp(args, "SHUTDOWN") == 0)
						{
							if (InitiateShutdown(nullptr, nullptr, 0, SHUTDOWN_POWEROFF, SHTDN_REASON_FLAG_PLANNED) != ERROR_SUCCESS)
							{
								printf("Shutdown error\n");
								std::system("shutdown /s");
							}
						}
						else if (strcmp(args, "RESTART") == 0)
						{
							PROCESS_INFORMATION pi;
							STARTUPINFO si;
							char exe_filename[256];
							GetModuleFileName(GetModuleHandle(nullptr), exe_filename, sizeof(exe_filename));
							GetStartupInfo(&si);
							if (!CreateProcess(exe_filename, nullptr, nullptr, nullptr,
								FALSE, CREATE_NEW_CONSOLE, nullptr, nullptr, &si, &pi))
							{
								printf("Process restart failed\n");
							}
							else
							{
								sock->close();
								connected = false;
								running = false;
								break;
							}
						}
					}
					else if (strcmp(cmd, "TASK") == 0)
					{
						int task_id = atoi(strtok(nullptr, ":"));
						char* operation = strtok(nullptr, ":");
						if (strcmp(operation, "RUN") == 0)
						{
							char* render_args = strtok(nullptr, "\a\r\n");
							printf("RUN task%d: %s\n", task_id, render_args);

							if (currentTask)
							{
								if (currentTask->running)
									currentTask->Kill();
								//delete currentTask;
							}

							currentTask = new MayaTask(task_id, maya_versions.front());
							if (currentTask->Start(render_args ? render_args : ""))
							{
								post_message("TASK:%d:STARTED", task_id);
							}
							else
							{
								//delete currentTask;
								currentTask = nullptr;
								post_message("TASK:%d:FAILED", task_id);
							}
						}
						else if (strcmp(operation, "KILL") == 0)
						{
							printf("KILL TASK\n");
							if (currentTask && currentTask->id == task_id)
							{
								if (currentTask->running)
									currentTask->Kill();
								//delete currentTask;
							}
						}
					}
					else if (strcmp(cmd, "CMD") == 0)
					{
						//printf("received: %s\n", args);
						//std::string out = exec(args);
						//post_message(out.c_str());
					}
				}
			}

			if (currentTask)
			{
				if (currentTask->running)
					currentTask->Kill();
				//delete currentTask;
				currentTask = nullptr;
			}

			connected = false;
			send_cv.notify_all();
			sender_thread.join();
			usage_thread.join();
		}
		else
		{
			//sock->cancel();
			sock->close();
			io_service.stop();
		}
	}

	return 0;
}

// Windows main entry point wrapper
int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
	LPWSTR *szArgList{ nullptr };
	int argc{ 0 };
	char** argv{ nullptr };

	szArgList = CommandLineToArgvW(GetCommandLineW(), &argc);
	if (szArgList == NULL)
	{
		MessageBox(NULL, "Unable to parse command line", "Error", MB_OK);
		return 10;
	}

	argv = new char*[argc + 1];
	for (int i = 0; i < argc; i++)
	{
		auto len = wcslen(szArgList[i]) + 1;
		argv[i] = new char[len];
		wcstombs(argv[i], szArgList[i], len);
	}

	LocalFree(szArgList);

	return main(argc, argv);
}
